﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerControl : MonoBehaviour
{
    public float speed;
    public Text CoinsText;
    public Text winText;
    public int jump;
    public float timeLeftForJump = 0.0f;
    public float maxJumpTime;

    private Rigidbody rb;
    private int Coins;
    private bool onGround;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        Coins = 0;
        SetCountText();
        winText.text = "";
    }

    void Update()
    {
        Timer();
        if (transform.position.y < -10)
        {
            RestartLevel();
        }

        Jump();

        onGround = Physics.Raycast(transform.position, Vector3.down, .52f);
    }

    void FixedUpdate()
    {
        Move();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pickup"))
        {
            other.gameObject.SetActive(false);
            Coins += 1;
            SetCountText();
        }
    }
    void SetCountText()
    {
        CoinsText.text = "Coins: " + Coins.ToString();
        if (Coins >= 12)
        {
            winText.text = "You Win!";
        }
    }

    private void Move()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);
    }

    private void Jump()
    {
        if (Input.GetButtonDown("Jump") && (timeLeftForJump == 0))
        {
            rb.velocity = Vector3.up * jump;
            timeLeftForJump = maxJumpTime;
        }
    }
    private void Timer()
    {
        timeLeftForJump -= Time.deltaTime;
        if (timeLeftForJump < 0)
        {
            timeLeftForJump = 0;
        }
    }

    void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
