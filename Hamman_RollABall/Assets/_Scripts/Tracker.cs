﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tracker : MonoBehaviour
{
    public GameObject[] allPickups;
    // Start is called before the first frame update
    public GameObject arrow;
    void Start()
    {
        allPickups = GameObject.FindGameObjectsWithTag("Pickup");//Search hierarchy and fill array with each pickup
    }

    // Update is called once per frame
    void Update()
    {
        FindClosestPickup();
    }
    void FindClosestPickup()
    {//This is called in Update()

        //Searches objects and finds closest object to this object:
        float minDistance = 10000000f;//Before we start searching make minDistance huge, so everything is shorter
        int closest = -1;//Make the closest thing -1 in case we don't find anything

        for (int i = 0; i < allPickups.Length; i++)//Loop through all pickups
        {
            if (allPickups[i].activeSelf)//Don't use if the pickup isn't active (already picked up)
            {
                float dist = Vector3.Distance(transform.position, allPickups[i].transform.position);//Get this pickup's distance
                if (dist < minDistance)//Is the pickup closer than the current closest pickup?
                {
                    minDistance = dist;//Then set min distance to my distance
                    closest = i;//Also set me as the one that's closest so you can use me later
                }
            }
        }

        if (closest != -1)//Don't use if we didn't find anything
        {
            //Uncomment this line with a filled GameObject reference to the arrow to use it:
            arrow.transform.LookAt(allPickups[closest].transform);//Point the arrow at the closest pickup
        }
    }
}
