﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AdjustSlider : MonoBehaviour
{
    public PlayerControl pc;
    private Slider slider;
     
    // Start is called before the first frame update
    void Start()
    {
        slider = GetComponent<Slider>();
        slider.maxValue = pc.maxJumpTime;
    }

    // Update is called once per frame
    void Update()
    {
        adjustSlider();
    }

    private void adjustSlider()
    {
        slider.value = pc.timeLeftForJump;
    }
}
